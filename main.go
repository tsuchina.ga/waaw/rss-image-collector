package main

import (
    "log"
    "net/http"
    "io/ioutil"
    "encoding/xml"
    "os"
    "strings"
    "time"
    "github.com/PuerkitoBio/goquery"
    "path"
    "sync"
    "io"
)

func main() {
    log.Println("hello, world")

    var wg sync.WaitGroup

    // rssの内容を取得してparse
    target := "https://www6.nhk.or.jp/special/feed/schedule.html"
    // resp, err := http.Get("https://news.yahoo.co.jp/pickup/computer/rss.xml")
    resp, err := http.Get(target)
    if err != nil {
        log.Fatalln(err)
    }
    defer resp.Body.Close()

    bytes, _ := ioutil.ReadAll(resp.Body)
    rss := new(Rss)
    if err = xml.Unmarshal(bytes, &rss); err != nil {
        log.Fatalln(err)
    }

    // channelを順番に処理する
    for _, channel := range rss.Channels {
        if channel.Title == "" {
            continue
        }

        // チャンネルディレクトリ作成
        channelDir := "save/" + strings.Replace(channel.Title, "/", " ", -1)
        if err := os.MkdirAll(channelDir, 0777); err != nil {
            log.Println(err)
            continue
        }

        // チャンネル内の記事を順番に処理する
        for _, item := range channel.Items {
            layout := "Mon, 02 Jan 2006 15:04:05 Z0700"
            t, _ := time.Parse(layout, item.PubDate)

            // 記事ディレクトリ作成
            saveDir := channelDir + "/" + t.Format("20060102150405") + " - " +
                strings.Replace(item.Title, "/", " ", -1)
            if _, err := os.Stat(saveDir); os.IsExist(err) {
                // すでに記事ディレクトリがある場合次の記事へ
                continue
            }

            if err := os.MkdirAll(saveDir, 0777); err != nil {
                // ディレクトリ作成失敗
                log.Println(err)
                continue
            }

            // 非同期で記事のhtmlをparseしてimgのsrcを取得し保存する無名関数
            wg.Add(1)
            go func () {
                // 記事のhtml取得
                url := item.Link
                resp, err := http.Get(url)
                if err != nil {
                    log.Println(err)
                    return
                }
                defer resp.Body.Close()

                doc, err := goquery.NewDocumentFromReader(resp.Body)
                if err != nil {
                    log.Fatalln(err)
                }

                doc.Find("img").Each(func(_ int, s *goquery.Selection) {
                    // img srcのurlから画像を取得して保存
                    url, _ := s.Attr("src")
                    if url != "" {

                        // 絶対パス・相対パスの場合の処理ができてない

                        filePath := saveDir + "/" + strings.Replace(path.Base(url), "/", " ", -1)
                        // ファイルが未作成の場合にのみ保存処理を行なう
                        if _, err = os.Stat(filePath); os.IsNotExist(err) {
                            wg.Add(1)
                            go func() {
                                file, err := os.Create(filePath)
                                if err != nil {
                                    log.Println(err)
                                    return
                                }
                                defer file.Close()

                                resp, err := http.Get(url)
                                if err != nil {
                                    log.Println(err)
                                    return
                                }
                                defer resp.Body.Close()

                                io.Copy(file, resp.Body)
                                wg.Done()
                            }()
                        }
                    }
                })
                wg.Done()
            }()
        }
    }

    wg.Wait()
    log.Println("bye, world")
}

type Rss struct {
    Channels []Channel `xml:"channel"`
}

type Channel struct {
    Title string `xml:"title"`
    Link string `xml:"link"`
    Description string `xml:"description"`
    Language string `xml:"language"`
    Copyright string `xml:"copyright"`
    ManagingEditor string `xml:"managingEditor"`
    WebMaster string `xml:"webMaster"`
    PubDate string `xml:"pubDate"`
    LastBuildDate string `xml:"lastBuildDate"`
    Category Category `xml:"category"`
    Generator string `xml:"generator"`
    Docs string `xml:"docs"`
    Cloud Cloud `xml:"cloud"`
    Ttl int `xml:"ttl"`
    Image Image `xml:"image"`
    Rating string `xml:"rating"`
    TextInput TextInput `xml:"textInput"`
    SkipHours int `xml:"skipHours"`
    SkipDays int `xml:"skipDays"`
    Items []Item `xml:"item"`
}

type Cloud struct {
    Text string `xml:",chardata"`
    Domain string `xml:"domain,attr"`
    Port int `xml:"port,attr"`
    Path string `xml:"path,attr"`
    RegisterProcedure string `xml:"registerProcedure,attr"`
    Protocol string `xml:"protocol,attr"`
}

type Image struct {
    Url string `xml:"url"`
    Title string `xml:"title"`
    Link string `xml:"link"`
    Width int `xml:"width"`
    Height int `xml:"height"`
    Description string `xml:"description"`
}

type TextInput struct {
    Title string `xml:"title"`
    Description string `xml:"description"`
    Name string `xml:"name"`
    Link string `xml:"link"`
}

type Item struct {
    Title string `xml:"title"`
    Link string `xml:"link"`
    Description string `xml:"description"`
    Author string `xml:"author"`
    Category Category `xml:"category"`
    Comments string `xml:"comments"`
    Enclosure Enclosure `xml:"enclosure"`
    Guid Guid `xml:"guid"`
    PubDate string `xml:"pubDate"`
    Source Source `xml:"source"`
}

type Category struct {
    Text string `xml:",chardata"`
    Domain string `xml:"domain,attr"`
}

type Enclosure struct {
    Text string `xml:",chardata"`
    Url string `xml:"url,attr"`
    Length string `xml:"length,attr"`
    Type string `xml:"type,attr"`
}

type Guid struct {
    Text string `xml:",chardata"`
    IsPermaLink bool `xml:"isPermaLink,attr"`
}

type Source struct {
    Text string `xml:",chardata"`
    Url string `xml:"url,attr"`
}
