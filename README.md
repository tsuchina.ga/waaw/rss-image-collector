# _WAAW07_ rss-image-collector

毎週1つWebアプリを作ろうの7回目。

期間は18/06/22 - 18/06/29

RSSを登録し、そこから自動で画像を集めるツールかサイトを作る。


## ゴール

* [x] RSSを解析できる
* [x] HTMLから画像を抽出できる
* [x] 抽出した画像をローカルに保存できる
* [ ] 保存する画像をフィルタリングできる


## 目的

* [x] rssの勉強
* [x] 画像ファイルの勉強


## 課題

* [x] xml(rssフォーマット)のパース
    * [XMLのパース/生成 - はじめてのGo言語](http://cuto.unirita.co.jp/gostudy/post/standard-library-xml/)
    * [RSS 2.0 のフォーマット｜RSS｜Web関連特集｜PHP & JavaScript Room](http://phpjavascriptroom.com/?t=topic&p=rss_format)
    * [RSS 2.0 Specification 日本語訳 - futomi's CGI Cafe](https://www.futomi.com/lecture/japanese/rss20.html)
    
* [x] httpリクエスト
    * [Goでhttpリクエストを送信する方法 - Qiita](https://qiita.com/taizo/items/c397dbfed7215969b0a5#1-%E3%82%B7%E3%83%B3%E3%83%97%E3%83%AB%E3%81%AAget%E3%83%AA%E3%82%AF%E3%82%A8%E3%82%B9%E3%83%88)

* [x] htmlのパース
    * [goqueryでお手軽スクレイピング！ - Qiita](https://qiita.com/yosuke_furukawa/items/5fd41f5bcf53d0a69ca6)

* [x] urlから画像の取得と保存
    * [golang で URLの画像データを取得して、ローカルのファイルに保存する術 - kitak's blog](https://kitak.hatenablog.jp/entry/2014/10/19/175133)
    * [path - The Go Programming Language](https://golang.org/pkg/path/)

* [x] 並行処理を利用した効率化
    * [Go の並行処理 - Block Rockin’ Codes](http://jxck.hatenablog.com/entry/20130414/1365960707)

* [ ] 絶対パス・相対パス

## 振り返り

0. Idea:

    アイデアやテーマについて
    * rssパーサなんてよくあるもの、そこから画像集めも比較的多いんじゃないかな？
    * プロトコルよりははるかに学びやすいフォーマット
    * 画像集めも興味がおおいところではないかな？

0. What went right:

    成功したこと・できたこと
    * rssのパース(xmlのmapping)
    * 画像のパスの取得(htmlのparse)

0. What went wrong:

    失敗したこと・できなかったこと
    * 絶対パスや相対パスへの考慮
    * spaなどあとからjavascriptで仮想DOMを埋め込むタイプのページのparse

0. What I learned:

    学べたこと
    * htmlのparseに便利なライブラリがあるということ
    * 非同期のためのgroup管理


## サンプル

なし
